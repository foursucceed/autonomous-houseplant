import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/models/plant_user.dart';
import 'package:autopot_app/screens/overview.dart';
import 'package:autopot_app/screens/sign_in.dart';
import 'package:autopot_app/services/auto_pot_service.dart';
import 'package:autopot_app/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'screens/navigation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // Interpreter modelInterpreter = await ModelService.loadModel();
  // await ModelService.predict(modelInterpreter);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider.value(
      value: UserService().userStream,
      child: Builder(
        builder: (context) => StreamProvider<PlantUser>.value(
          value: UserService(uid: (Provider.of<User>(context)?.uid ?? null))
                  .plantUser ??
              null,
          catchError: (_, __) => null,
          child: StreamProvider<List<AutoPot>>.value(
            value: AutoPotService((Provider.of<User>(context)?.uid ?? null))
                    .autoPots ??
                null,
            child: MaterialApp(
              title: 'AutoPot',
              theme: ThemeData(
                fontFamily: 'Lato',
                primarySwatch: createMaterialColor(Color(0xff545A37)),
                cardTheme: CardTheme(
                  shape: RoundedRectangleBorder(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(12.0),
                    ),
                  ),
                ),
              ),
              home: Wrapper(),
            ),
          ),
        ),
      ),
    );
  }

  MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }
}

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _user = Provider.of<PlantUser>(context);
    return _user == null ? SignIn() : MyNavigationBar();
  }
}

class MyNavigationBar extends StatefulWidget {
  @override
  _MyNavigationBarState createState() => _MyNavigationBarState();
}

class _MyNavigationBarState extends State<MyNavigationBar> {
  int _selectedIndex = 0;

  void _selectTab(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static List<Widget> _widgetOptions = <Widget>[Overview(), Navigation()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xff545A37),
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white,
        currentIndex: _selectedIndex,
        onTap: _selectTab,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Image.asset('assets/Chart_inactive.png', scale: 4),
              activeIcon: Image.asset('assets/Chart_active.png', scale: 4),
              label: 'Overview'),
          BottomNavigationBarItem(
              icon: Image.asset('assets/Discovery_inactive.png', scale: 4),
              activeIcon: Image.asset('assets/Discovery_active.png', scale: 4),
              label: 'Navigation'),
        ],
      ),
    );
  }
}
