import 'package:autopot_app/models/plant_user.dart';
import 'package:autopot_app/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AccountSettings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    User user = Provider.of<User>(context, listen: false);
    PlantUser plantUser = Provider.of<PlantUser>(context);

    final UserService _userService = UserService(uid: user?.uid);
    final _formkey = GlobalKey<FormState>();
    bool loading = false;

    String name = plantUser.name;
    String email = plantUser.email;
    String password;


    return Scaffold(
      appBar: AppBar(
        title: Text('Account Settings'),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(32),
        child: Form(
          key: _formkey,
          child: Column(
            children: [
              SizedBox(height: 16),
              TextFormField( // name
                initialValue: plantUser.name,
                onChanged: (val) {
                  name = val;
                },
                validator: (value) => value.isEmpty ? 'Please enter your first name' : null,
                decoration: InputDecoration(
                  labelText: 'First name',
                  border: OutlineInputBorder(),
                  suffixIcon: Icon(
                    Icons.error,
                  ),
                ),
              ),
              SizedBox(height: 16),
              TextFormField( // email
                initialValue: plantUser.email,
                onChanged: (val) {
                  email = val;
                },
                validator: (value) => value.isEmpty ? 'Please enter your email' : null,
                decoration: InputDecoration(
                  labelText: 'Email',
                  border: OutlineInputBorder(),
                  suffixIcon: Icon(
                    Icons.error,
                  ),
                ),
              ),
              SizedBox(height: 16),
              TextFormField( // password
                obscureText: true,
                onChanged: (val) {
                  password = val;
                },
                //validator: (value) => value.isEmpty ? 'Please enter your password' : null,
                decoration: InputDecoration(
                  labelText: 'Password',
                  border: OutlineInputBorder(),
                  suffixIcon: Icon(
                    Icons.error,
                  ),
                ),
              ),
              SizedBox(height: 40),
              Container(
                width: double.infinity,
                height: 56,
                child: ElevatedButton(
                  child: Text('Update account'),
                  onPressed: () async {
                    if (_formkey.currentState.validate()) {
                      await _userService.updateUserData(email, name);
                      Navigator.pop(context);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
