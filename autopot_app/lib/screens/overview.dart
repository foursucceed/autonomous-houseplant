import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/models/plant_user.dart';
import 'file:///E:/Projects/Autonomous%20Houseplant/autopot_app/lib/screens/charts/chart_view.dart';
import 'package:autopot_app/screens/charts/air_temperature_chart_multi.dart';
import 'package:autopot_app/screens/charts/amount_of_watering_chart_multi.dart';
import 'package:autopot_app/screens/charts/light_level_chart_multi.dart';
import 'package:autopot_app/screens/charts/plant_health_chart_multi.dart';
import 'package:autopot_app/screens/charts/relative_humidity_chart_multi.dart';
import 'package:autopot_app/screens/charts/soil_moisture_chart_multi.dart';
import 'package:autopot_app/screens/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Overview extends StatefulWidget {
  @override
  _OverviewState createState() => _OverviewState();
}

class _OverviewState extends State<Overview> {
  @override
  Widget build(BuildContext context) {
    PlantUser plantUser = Provider.of<PlantUser>(context);
    List<AutoPot> autoPots = Provider.of<List<AutoPot>>(context);
    if (plantUser != null && autoPots != null) {
      return Container(
          alignment: Alignment.topLeft,
          padding: EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 24, 0, 16),
                  child: Center(child: Image.asset('assets/icon.png', width: 24)),
                ),
                Text(
                  'Hi, ${plantUser.name}',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32),
                ),
                SizedBox(height: 16),
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text('Today',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.black.withOpacity(0.7))),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: IntrinsicHeight(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text('Plant care'),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(' 83%',
                                        style: TextStyle(
                                            fontSize: 28,
                                            fontWeight: FontWeight.bold,
                                      color: Color(0xffBECD94))),
                                  )
                                ],
                              ),
                              VerticalDivider(width: 12),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text('Watering'),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(' 21 ml',
                                        style: TextStyle(
                                            fontSize: 28,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff5E77B9))),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ChartView(SoilMoistureChartMulti(autoPots), autoPots,
                    title: 'Soil moisture (%)'),
                ChartView(AirTemperatureChartMulti(autoPots), autoPots,
                    title: 'Air temperature (C)'),
                ChartView(RelativeHumidityChartMulti(autoPots), autoPots,
                    title: 'Relative humidity (%)'),
                ChartView(LightLevelChartMulti(autoPots), autoPots,
                    title: 'Light level (lumen)'),
                ChartView(PlantHealthChartMulti(autoPots), autoPots, title: 'Predicted plant health (%)'),
                ChartView(AmountOfWateringChartMulti(autoPots), autoPots, title: 'Predicted amount of watering (ml)'),
              ],
            ),
          ));
    } else {
      return Loading();
    }
  }
}
