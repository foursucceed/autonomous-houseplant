import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/screens/auto_pot_settings.dart';
import 'package:autopot_app/screens/charts/air_temperature_chart_multi.dart';
import 'package:autopot_app/screens/charts/amount_of_watering_chart_multi.dart';
import 'package:autopot_app/screens/charts/light_level_chart_multi.dart';
import 'package:autopot_app/screens/charts/plant_health_chart_multi.dart';
import 'package:autopot_app/screens/charts/relative_humidity_chart_multi.dart';
import 'package:autopot_app/screens/charts/soil_moisture_chart_multi.dart';
import 'package:flutter/material.dart';

import 'charts/chart_view.dart';

class AutoPotDetails extends StatefulWidget {
  AutoPot autoPot;

  AutoPotDetails(this.autoPot);

  @override
  _AutoPotDetailsState createState() => _AutoPotDetailsState(autoPot);
}

class _AutoPotDetailsState extends State<AutoPotDetails> {
  AutoPot autoPot;

  _AutoPotDetailsState(this.autoPot);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pot details'),
        actions: [
          TextButton.icon(
              icon: Icon(Icons.settings,
              color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => AutoPotSettings(autoPot)));
              },
              label: Text(
                'Settings',
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Plant profile',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 8, 8),
                child: Row(children: <Widget>[
                  Text("Pot name: ",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(autoPot?.name ?? 'Unknown')
                ]),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 8, 8),
                child: Row(children: <Widget>[
                  Text("Plant scientific name: ",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(autoPot?.plant?.nameScientific ?? 'Unknown')
                ]),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 8, 8),
                child: Row(children: <Widget>[
                  Text("Registered: ",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(autoPot?.dateBought ?? 'Unknown')
                ]),
              ),
              SizedBox(height: 16),
              Text('Data',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
              SizedBox(height: 8),
              ChartView(SoilMoistureChartMulti([autoPot]), [autoPot],
                  title: 'Soil moisture (%)'),
              ChartView(AirTemperatureChartMulti([autoPot]), [autoPot],
                  title: 'Air temperature (C)'),
              ChartView(RelativeHumidityChartMulti([autoPot]), [autoPot],
                  title: 'Relative humidity (%)'),
              ChartView(LightLevelChartMulti([autoPot]), [autoPot],
                  title: 'Light level (lumen)'),
              ChartView(PlantHealthChartMulti([autoPot]), [autoPot],
                  title: 'Predicted plant health (%)'),
              ChartView(AmountOfWateringChartMulti([autoPot]), [autoPot],
                  title: 'Predicted amount of watering (ml)'),
            ],
          ),
        ),
      ),
    );
  }
}
