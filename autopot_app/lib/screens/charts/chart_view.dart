import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/screens/charts/indicator.dart';
import 'package:flutter/material.dart';

class ChartView extends StatelessWidget {
  List<AutoPot> autoPots = [];
  Widget widget;
  String title;

  ChartView(this.widget, this.autoPots, {String this.title});

  @override
  Widget build(BuildContext context) {
    return Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            ListTile(
              title: Text(title ?? 'No title',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      color: Colors.black.withOpacity(0.7))),
              subtitle: showSubTitle(),
              //trailing: IconButton(onPressed: () {}, icon: Icon(Icons.aspect_ratio)),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: widget,
            ),
          ],
        ));
  }

  Widget showSubTitle() {
    if (autoPots.length > 1) {
      return Wrap(
        direction: Axis.horizontal,
        spacing: 8,
        children: autoPots
            .map((autoPot) => Indicator(
                  color: Color(autoPot.color),
                  text: autoPot.name,
                  isSquare: false,
                  size: 10,
                  textColor: Colors.grey,
                ))
            .toList(),
      );
    } else {
      return null;
    }
  }
}
