import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/models/sensor_measurement.dart';
import 'package:autopot_app/screens/loading.dart';
import 'package:autopot_app/services/model_service.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:tflite_flutter/tflite_flutter.dart';

class PlantHealthChartMulti extends StatefulWidget {
  List<AutoPot> autoPots;

  PlantHealthChartMulti(this.autoPots);

  @override
  State<StatefulWidget> createState() => PlantHealthChartMultiState();
}

class PlantHealthChartMultiState extends State<PlantHealthChartMulti> {
  bool loaded = false;
  List<AutoPot> autoPots;

  @override
  void initState() {
    super.initState();
    autoPots = widget.autoPots;
    linesBarData();
  }

  @override
  Widget build(BuildContext context) {
    if (loaded) {
      return Container(
        height: 120,
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                    child: LineChart(
                      sampleData(),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ],
        ),
      );
    } else {
      return Loading();
    }
  }

  LineChartData sampleData() {
    return LineChartData(
      lineTouchData: LineTouchData(
        touchCallback: (LineTouchResponse touchResponse) {},
        handleBuiltInTouches: true,
      ),
      gridData: FlGridData(
        show: false,
      ),
      titlesData: FlTitlesData(
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          margin: 10,
          getTitles: (value) {
            switch (value.toInt()) {
              case 0:
                return 'Mon';
              case 1:
                return 'Tue';
              case 2:
                return 'Wed';
              case 3:
                return 'Thu';
              case 4:
                return 'Fri';
              case 5:
                return 'Sat';
              case 6:
                return 'Sun';
            }
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          interval: 20,
          margin: 8,
          reservedSize: 16,
        ),
      ),
      maxY: 100,
      minY: 0,
      lineBarsData: lines,
    );
  }

  List<LineChartBarData> lines = [];

  Future<List<LineChartBarData>> linesBarData() async {
    for (AutoPot autoPot in autoPots) {
      LineChartBarData lineChartBarData = LineChartBarData(
        spots: getDataPoints(
            autoPot.measurements, await loadDataPoints(autoPot.measurements)),
        isCurved: true,
        colors: [
          autoPots.length == 1 ? Color(0xfff7cb4d) : Color(autoPot.color)
        ],
        barWidth: 2,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: false,
        ),
      );
      lines.add(lineChartBarData);
    }
    setState(() {
      loaded = true;
    });
  }

  List<FlSpot> getDataPoints(
      List<SensorMeasurement> measurements, List output) {
    List<FlSpot> points = [];
    for (int i = 0; i < measurements.length; i++) {
      points.add(FlSpot(i.toDouble() / 24, output[i][0][1]));
    }
    return points;
  }

  Future<List> loadDataPoints(List<SensorMeasurement> measurements) async {
    Interpreter modelInterpreter = await ModelService.loadModel();
    List output = [];
    for (SensorMeasurement sm in measurements) {
      var input = [
        sm.soilMoisture,
        sm.airTemperature,
        sm.humidity,
        sm.lumen,
        sm.plantAgeInDays.toDouble()
      ];
      output.add(await ModelService.predict(modelInterpreter, input));
    }
    return output;
  }
}
