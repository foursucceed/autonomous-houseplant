import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/models/sensor_measurement.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class LightLevelChartMulti extends StatefulWidget {
  List<AutoPot> autoPots;

  LightLevelChartMulti(this.autoPots);

  @override
  State<StatefulWidget> createState() => LightLevelChartMultiState();
}

class LightLevelChartMultiState extends State<LightLevelChartMulti> {
  @override
  void initState() {
    super.initState();
  }

  List<AutoPot> autoPots;

  @override
  Widget build(BuildContext context) {
    autoPots = widget.autoPots;
    return Container(
      height: 120,
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                  child: LineChart(
                    sampleData(),
                    swapAnimationDuration: const Duration(milliseconds: 250),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ],
      ),
    );
  }

  LineChartData sampleData() {
    return LineChartData(
      lineTouchData: LineTouchData(
        touchCallback: (LineTouchResponse touchResponse) {},
        handleBuiltInTouches: true,
      ),
      gridData: FlGridData(
        show: false,
      ),
      titlesData: FlTitlesData(
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          margin: 10,
          getTitles: (value) {
            switch (value.toInt()) {
              case 0:
                return 'Mon';
              case 1:
                return 'Tue';
              case 2:
                return 'Wed';
              case 3:
                return 'Thu';
              case 4:
                return 'Fri';
              case 5:
                return 'Sat';
              case 6:
                return 'Sun';
            }
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          interval: 4,
          margin: 8,
          reservedSize: 16,
        ),
      ),
      maxY: 20,
      minY: 0,
      lineBarsData: linesBarData(),
    );
  }

  List<LineChartBarData> linesBarData() {
    List<LineChartBarData> lines = [];
    for (AutoPot autoPot in autoPots) {
      LineChartBarData lineChartBarData = LineChartBarData(
        spots: getDataPoints(autoPot.measurements),
        isCurved: true,
        colors: [autoPots.length == 1? Color(0xff63d297):Color(autoPot.color)],
        barWidth: 2,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: false,
        ),
      );
      lines.add(lineChartBarData);
    }
    return lines;
  }

  List<FlSpot> getDataPoints(List<SensorMeasurement> measurements) {
    List<FlSpot> points = [];
    for (int i = 0; i < measurements.length; i++) {
      points.add(FlSpot(i.toDouble() / 24, measurements[i].lumen));
    }
    return points;
  }
}
