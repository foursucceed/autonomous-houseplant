import 'package:autopot_app/screens/loading.dart';
import 'package:autopot_app/screens/sign_up.dart';
import 'package:autopot_app/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final UserService _userService = UserService();
  final _formkey = GlobalKey<FormState>();
  bool loading = false;

  String email;
  String password;

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            body: SingleChildScrollView(
              child: Form(
                key: _formkey,
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(32),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 32),
                      Image.asset('assets/logo.png', width: 200),
                      SizedBox(height: 48),
                      Text(
                        'Login',
                        style: TextStyle(fontSize: 34, fontWeight: FontWeight.bold, color: Color(0xff36391D)),
                      ),
                      SizedBox(height: 16),
                      Text('Welcome back',
                        style: TextStyle(fontSize: 14, color: Color(0xff36391D)),),
                      SizedBox(height: 40),
                      TextFormField(
                        // Email
                        onChanged: (val) {
                          email = val;
                        },
                        validator: (value) =>
                            value.isEmpty ? 'Please enter your email' : null,
                        decoration: InputDecoration(
                          labelText: 'Email',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.error,
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      TextFormField(
                        // Password
                        obscureText: true,
                        onChanged: (val) {
                          password = val;
                        },
                        validator: (value) =>
                            value.isEmpty ? 'Please enter your password' : null,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.error,
                          ),
                        ),
                      ),
                      Container(
                        alignment: Alignment.topRight,
                        child: TextButton(
                          child: Text("Forgot password?",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          onPressed: () => Navigator.push(context,
                              MaterialPageRoute(builder: (context) => SignUp())),
                        ),
                      ),
                      SizedBox(height: 40),
                      Container(
                        width: double.infinity,
                        height: 56,
                        child: ElevatedButton(
                          child: Text('Sign In'),
                          onPressed: () async {
                            if (_formkey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });
                              User user = await _userService
                                  .signInWithEmailAndPassword(email, password);
                              setState(() {
                                loading = false;
                              });
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      TextButton(
                        child: Text("Don't have an account? Create account",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        onPressed: () => Navigator.push(context,
                            MaterialPageRoute(builder: (context) => SignUp())),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
