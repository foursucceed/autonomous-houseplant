import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/screens/charts/air_temperature_chart_multi.dart';
import 'package:autopot_app/screens/charts/amount_of_watering_chart_multi.dart';
import 'package:autopot_app/screens/charts/light_level_chart_multi.dart';
import 'package:autopot_app/screens/charts/plant_health_chart_multi.dart';
import 'package:autopot_app/screens/charts/relative_humidity_chart_multi.dart';
import 'package:autopot_app/screens/charts/soil_moisture_chart_multi.dart';
import 'package:autopot_app/services/auto_pot_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AutoPotSettings extends StatefulWidget {
  AutoPot autoPot;

  AutoPotSettings(this.autoPot);

  @override
  _AutoPotSettingsState createState() => _AutoPotSettingsState(autoPot);
}

class _AutoPotSettingsState extends State<AutoPotSettings> {
  AutoPot autoPot;
  List _autonomousValues = [
    ['Watering', false],
    ['Moisture', false],
    ['Humidifier', false],
    ['Lumen', false],
    ['Fertilizer', false],
    ['Temperature', false]
  ];
  List _trackingValues = [
    ['High/low Temperature', false],
    ['High/low Humidity', false],
    ['High/low Lumen', false]
  ];

  _AutoPotSettingsState(this.autoPot);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pot settings'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Products',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
              ListTile(
                trailing: Icon(Icons.arrow_forward),
                title: Text('Pot / Connection'),
                onTap: () {},
              ),
              ListTile(
                trailing: Icon(Icons.arrow_forward),
                title: Text('Configure pairs'),
                onTap: () {},
              ),
              SizedBox(height: 16),
              Text('Autonomous settings',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
              for (int i = 0; i < _autonomousValues.length; i++)
                ListTile(
                  trailing: Switch(
                    value: _autonomousValues[i][1],
                    onChanged: (value) {
                      setState(() {
                        _autonomousValues[i][1] = !_autonomousValues[i][1];
                      });
                    },
                  ),
                  title: Text(_autonomousValues[i][0]),
                  onTap: () {},
                ),
              SizedBox(height: 16),
              Text('Tracking settings',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
              for (int i = 0; i < _trackingValues.length; i++)
                ListTile(
                  trailing: Switch(
                    value: _trackingValues[i][1],
                    onChanged: (value) {
                      setState(() {
                        _trackingValues[i][1] = !_trackingValues[i][1];
                      });
                    },
                  ),
                  title: Text(_trackingValues[i][0]),
                  onTap: () {},
                ),
              SizedBox(height: 16),
              Container(
                width: double.infinity,
                child: ElevatedButton(
                  child: Text('Remove plant',
                  style: TextStyle(
                    color: Colors.white
                  ),
                  ),
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.red)),
                  onPressed: () async {
                    await AutoPotService(
                            Provider.of<User>(context, listen: false)?.uid)
                        .removeAutoPot(autoPot);
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
