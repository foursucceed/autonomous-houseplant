import 'package:autopot_app/models/plant_user.dart';
import 'package:autopot_app/screens/loading.dart';
import 'package:autopot_app/services/user_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final UserService _userService = UserService();
  final _formkey = GlobalKey<FormState>();
  bool loading = false;

  String email;
  String password;

  String name;

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            body: SingleChildScrollView(
              child: Form(
                key: _formkey,
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(32),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 32),
                      Image.asset('assets/logo.png', width: 200),
                      SizedBox(height: 48),
                      Text(
                        'Create account',
                        style: TextStyle(
                            fontSize: 34,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff36391D)),
                      ),
                      SizedBox(height: 16),
                      Text(
                        'To sign up, add name, your email and password',
                        style:
                            TextStyle(fontSize: 14, color: Color(0xff36391D)),
                      ),
                      SizedBox(height: 40),
                      TextFormField(
                        // Name
                        onChanged: (val) {
                          name = val;
                        },
                        validator: (value) => value.isEmpty
                            ? 'Please enter your name'
                            : null,
                        decoration: InputDecoration(
                          labelText: 'Name',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.error,
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      TextFormField(
                        // Email
                        onChanged: (val) {
                          email = val;
                        },
                        validator: (value) =>
                            value.isEmpty ? 'Please enter your email' : null,
                        decoration: InputDecoration(
                          labelText: 'Email',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.error,
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      TextFormField(
                        // Password
                        obscureText: true,
                        onChanged: (val) {
                          password = val;
                        },
                        validator: (value) =>
                            value.isEmpty ? 'Please enter your password' : null,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          border: OutlineInputBorder(),
                          suffixIcon: Icon(
                            Icons.error,
                          ),
                        ),
                      ),
                      SizedBox(height: 40),
                      Container(
                        width: double.infinity,
                        height: 56,
                        child: ElevatedButton(
                          child: Text('Create account'),
                          onPressed: () async {
                            if (_formkey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });
                              PlantUser plantUser = PlantUser(
                                  email: email,
                                  password: password,
                                  name: name);
                              User user = await _userService
                                  .signUpWithEmailAndPassword(plantUser);
                              if (user != null) Navigator.pop(context);
                              setState(() {
                                loading = false;
                              });
                            }
                          },
                        ),
                      ),
                      SizedBox(height: 16),
                      Image.asset('assets/agree_to_terms.png'),
                      SizedBox(
                        height: 32,
                      ),
                      TextButton(
                        child: Text("Already have an account? Sign In",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        onPressed: () => Navigator.pop(context),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
