import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/screens/auto_pot_details.dart';
import 'package:autopot_app/screens/profile.dart';
import 'package:autopot_app/screens/shop.dart';
import 'package:autopot_app/services/auto_pot_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Navigation extends StatefulWidget {
  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  String autoPotName;

  @override
  Widget build(BuildContext context) {
    List<AutoPot> autoPots = Provider.of<List<AutoPot>>(context) ?? [];
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 24, 0, 16),
            child: Center(child: Image.asset('assets/icon.png', width: 24)),
          ),
          Text(
            'Navigation',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32),
          ),
          Container(
            height: 140,
            child: GridView.count(
              primary: false,
              crossAxisCount: 3,
              children: <Widget>[
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 72,
                        width: 72,
                        child: InkWell(
                          customBorder: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(24)),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Profile()));
                          }, // Handle your callback.
                          splashColor: Colors.white.withOpacity(0.2),
                          child: Ink(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(24)),
                              image: DecorationImage(
                                image: AssetImage('assets/profile_image.jpg'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 8),
                      Text('Profile')
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 72,
                        width: 72,
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Shop()));
                          },
                          child: Image.asset('assets/Buy.png', scale: 3),
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Color(0xff36391D)),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(24))),
                          ),
                        ),
                      ),
                      SizedBox(height: 8),
                      Text('Shop')
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 72,
                        width: 72,
                        child: ElevatedButton(
                          onPressed: () => showModalBottomSheet(
                            context: context,
                            builder: (context) => Container(
                              child: Padding(
                                padding: const EdgeInsets.all(32.0),
                                child: Wrap(
                                  direction: Axis.horizontal,
                                  alignment: WrapAlignment.center,
                                  runAlignment: WrapAlignment.center,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  verticalDirection: VerticalDirection.down,
                                  runSpacing: 8,
                                  spacing: 8,
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Setup pot',
                                        style: TextStyle(fontSize: 24),
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                    TextFormField(
                                      // Email
                                      onChanged: (val) {
                                        autoPotName = val;
                                      },
                                      validator: (value) => value.isEmpty
                                          ? 'Please enter your pot name'
                                          : null,
                                      decoration: InputDecoration(
                                        labelText: 'Pot name',
                                        border: OutlineInputBorder(),
                                        suffixIcon: Icon(
                                          Icons.error,
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                    SizedBox(
                                      width: double.infinity,
                                      height: 48,
                                      child: TextButton(
                                          onPressed: () async {
                                            await AutoPotService(
                                                    Provider.of<User>(context,
                                                            listen: false)
                                                        ?.uid)
                                                .addAutoPot(
                                                    name: autoPotName,
                                                    dateBought: 'Unknown',
                                                    loadCsvFile:
                                                        'test_set_plant_1',
                                                    color: 0xfff2003b);
                                            Navigator.pop(context);
                                          },
                                          child: Text('Continue',
                                              style: TextStyle(
                                                  color: Colors.black
                                                      .withOpacity(0.7))),
                                          style: ButtonStyle(
                                              shape: MaterialStateProperty.all<
                                                      RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              24),
                                                      side: BorderSide(
                                                          color: Colors.black
                                                              .withOpacity(0.7)))))),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          child: Image.asset('assets/pluss.png', scale: 3),
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Color(0xff36391D)),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(24))),
                          ),
                        ),
                      ),
                      SizedBox(height: 8),
                      Text('Setup pot')
                    ],
                  ),
                ),
              ],
            ),
          ),
          Divider(height: 40),
          Text(
            'Your pots',
            style: TextStyle(
                color: Colors.black.withOpacity(0.7),
                fontWeight: FontWeight.bold,
                fontSize: 14),
          ),
          Expanded(
              child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 16,
                crossAxisSpacing: 16,
                childAspectRatio: 0.8),
            itemCount: autoPots.length,
            itemBuilder: (context, index) {
              return AutoPotItem(autoPots[index]);
            },
          )),
        ],
      ),
    );
  }
}

class AutoPotItem extends StatelessWidget {
  AutoPot autoPot;

  AutoPotItem(this.autoPot);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 72,
            width: 72,
            child: potWidget(context, true),
          ),
          SizedBox(height: 8),
          Flexible(
              child: Text(
            autoPot?.name ?? 'error_loading',
            softWrap: true,
            textAlign: TextAlign.center,
          ))
        ],
      ),
    );
  }

  Widget potWidget(BuildContext context, bool isImage) {
    if (!isImage) {
      return ElevatedButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AutoPotDetails(autoPot)));
        },
        child: Icon(
          Icons.grass,
          color: Colors.white,
          size: 32,
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Color(0xff36391D)),
          shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24))),
        ),
      );
    } else {
      return InkWell(
        customBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AutoPotDetails(autoPot)));
        },
        splashColor: Colors.white.withOpacity(0.2),
        child: Ink(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(24)),
            image: DecorationImage(
              image: AssetImage('assets/plant1.jpeg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
      );
    }
  }
}
