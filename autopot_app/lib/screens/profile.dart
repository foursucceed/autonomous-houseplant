import 'package:autopot_app/models/plant_user.dart';
import 'package:autopot_app/screens/account.dart';
import 'package:autopot_app/services/user_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {

    PlantUser plantUser = Provider.of<PlantUser>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 16,
          ),
          Column(children: [
            SizedBox(
              height: 120,
              width: 120,
              child: InkWell(
                customBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
                onTap: () {}, // Handle your callback.
                splashColor: Colors.white.withOpacity(0.2),
                child: Ink(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(24)),
                    image: DecorationImage(
                      image: AssetImage('assets/profile_image.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text('${plantUser?.name}', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black.withOpacity(0.7)))
          ]),
          SizedBox(
            height: 24,
          ),
          ListTile(
            leading: Image.asset('assets/Account.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            trailing: Image.asset('assets/Right_Arrow.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            title: Text('Account'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AccountSettings()));
            },
          ),
          ListTile(
            leading: Image.asset('assets/System.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            trailing: Image.asset('assets/Right_Arrow.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            title: Text('System'),
            onTap: () {},
          ),
          ListTile(
            leading: Image.asset('assets/Pref.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            trailing: Image.asset('assets/Right_Arrow.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            title: Text('App preferences'),
            onTap: () {},
          ),
          ListTile(
            leading: Image.asset('assets/Support.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            trailing: Image.asset('assets/Right_Arrow.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            title: Text('Support'),
            onTap: () {},
          ),
          ListTile(
            leading: Image.asset('assets/Shield_Done.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            trailing: Image.asset('assets/Right_Arrow.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            title: Text('Data and safety'),
            onTap: () {},
          ),
          ListTile(
            leading: Image.asset('assets/Terms.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            trailing: Image.asset('assets/Right_Arrow.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            title: Text('Terms and conditions'),
            onTap: () {},
          ),
          ListTile(
            leading: Image.asset('assets/Help.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            trailing: Image.asset('assets/Right_Arrow.png', scale: 4, color: Colors.black.withOpacity(0.7)),
            title: Text('Help'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.login_outlined),
            title: Text(
              'Log out',
            ),
            onTap: () {
              UserService().signOut();
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
