import 'package:autopot_app/models/auto_pot.dart';
import 'package:autopot_app/models/plant.dart';
import 'package:autopot_app/models/sensor_measurement.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csv/csv.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

class AutoPotService {
  final FirebaseAuth __fireBaseAuth = FirebaseAuth.instance;
  CollectionReference autoPotCollection;
  CollectionReference plantCollection;

  AutoPotService(String uid) {
    autoPotCollection = FirebaseFirestore.instance
        .collection('users')
        .doc(uid)
        .collection('autoPots');
    plantCollection = FirebaseFirestore.instance.collection('plants');
  }

  Stream<User> get userStream {
    return __fireBaseAuth.authStateChanges();
  }

  // get pots
  Stream<List<AutoPot>> get autoPots {
    return autoPotCollection.snapshots().map(_autoPotFromSnapshot);
  }

  // add user pot
  Future addAutoPot(
      {String name, String nameScientific, String dateBought, int color, String loadCsvFile}) async {
    var plant = {'name': name, 'nameScientific': nameScientific, 'longivity': 20};
    List<SensorMeasurement> measurements;
    if (loadCsvFile.isNotEmpty) {
      measurements = await loadSimulatedData(loadCsvFile);
    }
    return await autoPotCollection.add({
      'name': name,
      'dateBought': dateBought,
      'plant': plant,
      'color': color,
      'measurements': measurements.map((i) => i.toMap()).toList()
    });
  }

  // update user pot
  Future updateAutoPot(String name, String dateBought) async {
    var plant = {'name': name, 'nameScientific': name, 'longivity': 20};
    return await autoPotCollection.doc().set({'name': name, 'plant': plant});
  }

  // remove user pot
  Future removeAutoPot(AutoPot autoPot) async {
    return await autoPotCollection.doc(autoPot.uid).delete();
  }

  List<AutoPot> _autoPotFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      Plant plant = Plant(
          name: doc.data()['plant']['name'],
          nameScientific: doc.data()['plant']['nameScientific'],
          longivity: doc.data()['plant']['longivity']);
      List measurements = doc.data()['measurements'];
      List<SensorMeasurement> sensorMeasurements = measurements
          .map<SensorMeasurement>(
              (element) => SensorMeasurement.fromMap(element))
          .toList();
      return AutoPot(
          uid: doc.id,
          name: doc.data()['name'],
          color: doc.data()['color'],
          dateBought: doc.data()['dateBought'],
          plant: plant,
          measurements: sensorMeasurements);
    }).toList();
  }

  Future<List<SensorMeasurement>> loadSimulatedData(String name) async {
    final data = await rootBundle.loadString("assets/${name}.csv");
    List<List<dynamic>> csvList = const CsvToListConverter().convert(data);
    List<SensorMeasurement> measurements = [];
    for (int i = 1; i < csvList.length; i++) {
      SensorMeasurement sensorMeasurement = SensorMeasurement(
          timestamp: csvList[i][2],
          soilMoisture: csvList[i][3].toDouble(),
          airTemperature: csvList[i][4].toDouble(),
          humidity: csvList[i][5].toDouble(),
          lumen: csvList[i][6].toDouble(),
          plantAgeInDays: csvList[i][7]);
      measurements.add(sensorMeasurement);
    }
    return measurements;
  }
}
