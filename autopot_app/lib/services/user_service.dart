import 'package:autopot_app/models/plant_user.dart';
import 'package:autopot_app/services/auto_pot_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserService {
  final FirebaseAuth __fireBaseAuth = FirebaseAuth.instance;
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  final String uid;

  UserService({this.uid});

  Stream<User> get userStream {
    return __fireBaseAuth.authStateChanges();
  }

  // update user data
  Future updateUserData(String email, String name) async {
    return await userCollection
        .doc(uid)
        .set({'name': name, 'email': email});
  }

  // Sign in
  Future<User> signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential result = await __fireBaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = result.user;
      return user;
    } catch (e) {
      print(e);
    }
  }

  // Sign up
  Future<User> signUpWithEmailAndPassword(PlantUser plantUser) async {
    try {
      // Create user
      UserCredential result =
          await __fireBaseAuth.createUserWithEmailAndPassword(
              email: plantUser.email, password: plantUser.password);
      User user = result.user;

      // Create user document
      await UserService(uid: user.uid).updateUserData(
          plantUser.email, plantUser.name);

      // Add user pot 1
      await AutoPotService(user.uid)
          .addAutoPot(name: 'Cali', nameScientific: 'Calanthea Orbifolia', dateBought: '11.04.2021', loadCsvFile: 'test_set_plant_1', color: 0xff840484);

      // Add user pot 2
      await AutoPotService(user.uid)
          .addAutoPot(name: 'Aloe Vera', nameScientific: 'Aloe Vera', dateBought: '11.04.2021', loadCsvFile: 'test_set_plant_2', color: 0xffF8A707);

      return user;
    } catch (e) {
      print(e);
    }
  }

  // Sign out
  Future<User> signOut() async {
    try {
      await __fireBaseAuth.signOut();
    } catch (e) {
      print(e);
    }
  }

  Stream<PlantUser> get plantUser {
    return userCollection
        .doc(uid)
        .snapshots()
        .map((snapshot) => _plantUserFromSnapshot(snapshot));
  }

  PlantUser _plantUserFromSnapshot(DocumentSnapshot doc) {
    var data = doc.data();
    return PlantUser(
        email: doc.data()['email'],
        password: null,
        name: data['name'],
        plants: data['plants']);
  }
}
