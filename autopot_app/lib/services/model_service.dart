import 'dart:async';
import 'dart:io';

import 'package:firebase_ml_custom/firebase_ml_custom.dart';
import 'package:tflite_flutter/tflite_flutter.dart';

class ModelService {

  static Future<Interpreter> loadModel() async {
    final modelFile = await loadModelFromFirebase();
    return loadTFLiteModel(modelFile);
  }

  /// Downloads custom model from the Firebase console and return its file.
  /// located on the mobile device.
  static Future<File> loadModelFromFirebase() async {
    try {
      // Create model with a name that is specified in the Firebase console
      final model = FirebaseCustomRemoteModel('AutoPot');
      final conditions = FirebaseModelDownloadConditions(
          androidRequireWifi: true, iosAllowCellularAccess: false);

      // Create model manager associated with default Firebase App instance.
      final modelManager = FirebaseModelManager.instance;

      // Begin downloading and wait until the model is downloaded successfully.
      await modelManager.download(model, conditions);
      assert(await modelManager.isModelDownloaded(model) == true);

      // Get latest model file to use it for inference by the interpreter.
      var modelFile = await modelManager.getLatestModelFile(model);
      assert(modelFile != null);
      return modelFile;
    } catch (exception) {
      print('Failed on loading your model from Firebase: $exception');
      print('The program will not be resumed');
      rethrow;
    }
  }

  /// Loads the model into some TF Lite interpreter.
  static Future<Interpreter> loadTFLiteModel(File modelFile) async {
    Interpreter interpreter = Interpreter.fromFile(modelFile);
    return interpreter;
  }

  static Future predict(Interpreter interpreter, List input) async {
    // Set output shape
    var output = List(2).reshape([1, 2]);

    // Predict
    interpreter.run(input, output);

    return output;
  }

}