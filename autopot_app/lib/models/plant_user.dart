import 'package:autopot_app/models/plant.dart';

class PlantUser {

  String uid;
  String name;
  String email;
  String password;
  List<Plant> plants;

  PlantUser(
      {this.email, this.password, this.name, this.plants});
}
