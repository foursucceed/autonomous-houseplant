class SensorMeasurement {
  String uid;
  int timestamp; // In minutes
  double soilMoisture; // Volumetric soil moisture (%)
  double airTemperature; // Air temperature in celsius (C)
  double humidity; // Relative humidity (%)
  double lumen; // Light level (lumen)
  int plantAgeInDays;

  SensorMeasurement(
      {this.uid,
      this.timestamp,
      this.soilMoisture,
      this.airTemperature,
      this.humidity,
      this.lumen,
      this.plantAgeInDays}); // plant age in days

  SensorMeasurement.fromMap(Map<String, dynamic> data) {
    uid = data['uid'];
    timestamp = data['timestamp'];
    soilMoisture = data['soilMoisture'];
    airTemperature = data['airTemperature'];
    humidity = data['humidity'];
    lumen = data['lumen'];
    plantAgeInDays = data['plantAgeInDays'];
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'timestamp': timestamp,
      'soilMoisture': soilMoisture,
      'airTemperature': airTemperature,
      'humidity': humidity,
      'lumen': lumen,
      'plantAgeInDays': plantAgeInDays,
    };
  }
}
