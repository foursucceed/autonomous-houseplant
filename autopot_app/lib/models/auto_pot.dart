import 'package:autopot_app/models/auto_board.dart';
import 'package:autopot_app/models/plant.dart';
import 'package:autopot_app/models/plant_instructions.dart';
import 'package:autopot_app/models/sensor_measurement.dart';

class AutoPot {
  String uid;
  String plantUserUid;

  Plant plant;
  AutoBoard autoBoard;
  List<int> healthStatus;
  List<SensorMeasurement> measurements;
  PlantInstructions plantInstructions;

  String name;
  int color;
  String dateBought;
  DateTime dateRegistered;

  AutoPot(
      {this.uid,
      this.plantUserUid,
      this.plant,
      this.autoBoard,
      this.healthStatus,
      this.measurements,
      this.plantInstructions,
      this.name,
      this.color,
      this.dateBought,
      this.dateRegistered});
}
